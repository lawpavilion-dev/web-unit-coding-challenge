# Introduction

Coding challenge for the LawPavilion's Web Unit (Senior Developer)

# Skills Required
- Laravel (PHP)
- HTML & CSS
- JavaScript
- API

# Instruction
Develop a mini Database Dump application. Kindly note the following:

- The application should list all the tables within the database alongside other table information you deem fit
- The application should inlcude a user registration and login system
- User should be able to select tables for dumping with a check
- Ability to dump the whole database should exist
- The application should be able to take database connection credentials
- The application should show database backup/dump history based on logged-in user
- The application should include options that enables the user download the dumps to their local machine or save to their Google Drive
